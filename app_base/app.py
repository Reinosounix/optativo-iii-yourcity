import urllib.request as requests
from flask import Flask

app = Flask(__name__)


@app.route('/')
def hello_world():
    response = requests.get('http://app_users:81/users')
    response = requests.get('http://app_municipality:82/municipalities')

    return response.text
