import string
import random
from project.municipalities.models import Municipality


def random_string():
    return ''.join(
        [
            random.choice(string.ascii_letters)
            for _ in range(16)
        ])


def create_municipality():
    return Municipality(
        name=random_string(),
        email='{}@a.cl'.format(random_string()),
        password=random_string(),
        phone_number=random_string()
    )
