from project.municipalities.models import Municipality
from project._tests.base import BaseTestCase


class TestCreate(BaseTestCase):
    def _call_create(self, data):
        with self.client:
            return self.client.post(
                '/municipalities',
                json=data
            )

    def test_create(self):
        data = {
            "name": "Hernann",
            "email": "a@a.cl",
            "password": "123456",
            "phone_number": "123456789",
        }

        self.assertEquals(0, Municipality.query.count())
        response = self._call_create(data)
        self.assertStatus(response, 201)
        self.assertEquals(1, Municipality.query.count())
        json_response = response.json
        self.assertEquals(json_response['name'], data['name'])
        self.assertEquals(json_response['email'], data['email'])
        self.assertEquals(json_response['phone_number'], data['phone_number'])
        self.assertNotIn('password', json_response)

    def test_create_without_name(self):
        data = {
            "email": "a@a.cl",
            "phone_number": "123456789",
        }

        response = self._call_create(data)
        self.assertStatus(response, 400)
        json_response = response.json
        self.assertIn('name', json_response)
        self.assertEquals(
            json_response['name'][0], 'Missing data for required field.')

    def test_create_without_email(self):
        data = {
            "name": "Hernann",
            "phone_number": "123456789",
        }

        response = self._call_create(data)
        self.assertStatus(response, 400)

    def test_create_without_password(self):
        data = {
            "name": "Hernann",
            "email": "a@a.cl",
            "phone_number": "123456789",
        }

        response = self._call_create(data)
        self.assertStatus(response, 400)

    def test_create_without_phonenumber(self):
        data = {
            "name": "Hernann",
            "email": "a@a.cl"
        }

        response = self._call_create(data)
        self.assertStatus(response, 400)
