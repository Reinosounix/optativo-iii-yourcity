from project import db
from project.municipalities.models import Municipality
from project._tests.base import BaseTestCase
from project.municipalities._tests.utils import create_municipality


class TestUpdate(BaseTestCase):
    def test_update(self):
        municipality = create_municipality()

        db.session.add(municipality)
        db.session.commit()

        data = {
            "name": "Actualizado",
            "email": "b@b.cl",
            "password": "12356",
            "phone_number": "123456789",
        }

        self.assertEquals(1, Municipality.query.count())
        with self.client:
            response = self.client.put(
                f'/municipalities/{municipality.id}',
                json=data
            )
        self.assertStatus(response, 200)
        self.assertEquals(1, Municipality.query.count())
        json_response = response.json
        self.assertEquals(json_response['name'], data['name'])
        self.assertEquals(json_response['email'], data['email'])
        self.assertEquals(json_response['phone_number'], data['phone_number'])
        updated_municipality = Municipality.query.filter_by(
            id=municipality.id).first()
        self.assertEquals(updated_municipality.name, data['name'])
        self.assertEquals(updated_municipality.email, data['email'])
        self.assertEquals(updated_municipality.phone_number,
                          data['phone_number'])

    def test_update_unexisting_municipality(self):
        data = {
            "name": "Actualizado",
            "email": "b@b.cl",
            "password": "123435",
            "phone_number": "123456789",
        }

        self.assertEquals(0, Municipality.query.count())
        with self.client:
            response = self.client.put(
                '/municipalities/1',
                json=data
            )
        self.assertStatus(response, 404)
        self.assertEquals(0, Municipality.query.count())
