from project import db
from project.municipalities.models import Municipality
from project._tests.base import BaseTestCase
from project.municipalities._tests.utils import create_municipality


class TestList(BaseTestCase):
    def test_list(self):
        municipality1 = create_municipality()
        municipality2 = create_municipality()
        db.session.add(municipality1)
        db.session.add(municipality2)
        db.session.commit()

        self.assertEquals(2, Municipality.query.count())
        with self.client:
            response = self.client.get(
                '/municipalities'
            )
        self.assertStatus(response, 200)
        json_response = response.json
        self.assertEquals(2, len(json_response))
        self.assertEquals(municipality1.name, json_response[0]['name'])
        self.assertEquals(municipality2.name, json_response[1]['name'])
